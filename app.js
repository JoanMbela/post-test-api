const express = require('express');
const PouchDB = require('pouchdb');
const app = express()

app.use(express.json());
const db = new PouchDB('post');


app.post('/store', async function (req, res) {
  var doc = {};
  const count = (await db.info()).doc_count
  doc._id = `post/${count}`;
  doc.name = req.body.name;
  doc.firstname = req.body.firstname;
  doc.age = req.body.age;

  await db.put(doc);
  res.send('connection enbale');
})

app.get('/post/:id', async function (req, res) {
  const count = (await db.info()).doc_count

  
  if (req.params.id > count-1) {
    res.send("Utilisateur inexistant. Recommecer avec une valeur reel.")
  } else {
    try {
      var doc = await db.get(`post/${req.params.id}`);
      res.send(doc);
    } catch (err) {
      res.sendStatus(404)
    }
  }
})


app.get('/', async function (req, res) {
  var b = await db.info()
  console.log(await b)
  var doc = await db.allDocs()
  res.send({res: doc})
})
app.listen(80)

